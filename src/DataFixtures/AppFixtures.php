<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

use App\Entity\Categorie;
use App\Entity\Produit;
use App\Entity\User;
//hasher mdp
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;


class AppFixtures extends Fixture
{
    public function __construct(private UserPasswordHasherInterface $passwordHasher)
    {}

    public function load(ObjectManager $manager): void
    {
        //user
        $user = new User();
        $user->setNom('user');
        $user->setEmail('user@gmail.com');
        $user->setPassword($this->passwordHasher->hashPassword($user, 'mdp'));
        // user (admin)
        $admin = new User();
        $admin->setNom('admin');
        $admin->setEmail('admin@gmail.com');
        $admin->setPassword($this->passwordHasher->hashPassword($admin, 'mdp'));
        $admin->setRoles(['ROLE_ADMIN']);

        $manager->persist($user);
        $manager->persist($admin);



        // categories
        $tabCategories = array();
        for ($i = 0; $i < 5; $i++) {
            $cat = new Categorie();
            $cat->setNom('categorie' . $i);
            $cat->setDescription(' description de la categorie' . $i);

            //ajout des participants
            array_push($tabCategories, $cat);

        }

        foreach ($tabCategories as $cat) {
            $manager->persist($cat);
        }
        // produits
        $tabProduits = array();

        for ($i = 0; $i < 21; $i++) {
            $prod = new Produit();
            $prod->setNom('produit' . $i);
            $prod->setDateAjout(new \DateTime(mt_rand(-120, +120) . ' day'));
            $prod->setImage($i . '.jpg');
            $prod->setDescription(' description du produit' . $i);

            if (!empty($tabCategories)) {
                $prod->addCategorie($tabCategories[rand(0, count($tabCategories) - 1)]);
            }
            array_push($tabProduits, $prod);
        }



        foreach ($tabProduits as $prod) {
            $manager->persist($prod);
        }




        $manager->flush();
    }
}
