<?php

namespace App\Controller\Admin;

use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
// configuration  admin
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use App\Controller\Admin\CategorieCrudController;
use App\Controller\Admin\ProduitCrudController;
use App\Entity\Categorie;
use App\Entity\Produit;
// modifier l'espace admin 
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        //return parent::index();

        // Option 1. You can make your dashboard redirect to some common page of your backend
        //
         $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
        return $this->redirect($adminUrlGenerator->setController(CategorieCrudController::class)->generateUrl());

        // Option 2. You can make your dashboard redirect to different pages depending on the user
        //
        // if ('jane' === $this->getUser()->getUsername()) {
        //     return $this->redirect('...');
        // }

        // Option 3. You can render some custom template to display a proper dashboard with widgets, etc.
        // (tip: it's easier if your template extends from @EasyAdmin/page/content.html.twig)
        //
        // return $this->render('some/path/my-dashboard.html.twig');
    }

    public function configureActions(): Actions
    {
        $actions = parent::configureActions();

        $actions->update(Crud::PAGE_INDEX, 'edit', function (Action $action) {
            return $action
                ->setIcon('fa fa-edit')
                ->setLabel('Edit');
        });

        $actions->update(Crud::PAGE_INDEX, 'delete', function (Action $action) {
            return $action
                ->setIcon('fa fa-trash')
                ->setLabel('Delete');
        });

        return $actions;
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Revision');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('categorie', 'fas fa-list', Categorie::class);
        yield MenuItem::linkToCrud('produit', 'fas fa-list', Produit::class);
        yield MenuItem::linkToRoute('Retour home', 'fa fa-home', 'app_home');

    }

    // enlever les trois petits points 

    public function configureCrud(): Crud
    {
        $crud =  parent::configureCrud();
        return $crud->showEntityActionsInlined();
    }

}
